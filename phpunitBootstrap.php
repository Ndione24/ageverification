<?php
/**
 * Include the autoload functionality to allow us to run the unit tests
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
require_once('vendor/autoload.php');
