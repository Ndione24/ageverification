<?php
/**
 * If the payload to pass to the AVS provider is not valid, prevent the call being made
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification\Exception;


class InvalidPayloadException extends \Exception
{
    protected $message = 'The provided payload isn\'t valid for the AVS provider.  ' .
        'please check the output of getAvsPayload() or run payloadIsValid() ahead of submitAgeVerification()';
}
