<?php
/**
 * Implementation to work with AgeChecked AVS provider (https://www.agechecked.com/)
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification;

use GaryBell\AgeVerification\Exception\InvalidAgeCheckedCredentialsException;
use GaryBell\AgeVerification\Exception\InvalidPayloadException;
use GaryBell\AgeVerification\Interfaces\AvsDataSubjectInterface;

class AgeChecked implements Interfaces\AgeVerificationInterface
{
    /**
     * @var string - the URL the API is at for verifying people's age. Needs a suffix adding to work
     * @see getEndpointSuffix()
     */
    protected string $endpoint = 'https://agechecked.com/api/acapiremote/';

    /**
     * Valid response statuses from AgeChecked. These are:
     *  - 6 (Approved)
     *  - 7 (Approved - user has passed verification previously)
     *  - 13 (Approved - but the date of birth did not match)
     * @var array|int[]
     */
    protected array $validAvsStatuses = [6, 7, 13];

    /**
     * @var string The secret key. Used to authenticate against the service
     */
    private string $secretKey = '';

    /**
     * @var array Payload to use for the AVS call
     */
    private array $payload = [];

    /**
     * Whether or not the data subject is age verified following the submitAgeVerification call
     * @var bool
     */
    private bool $ageVerified = false;

    /**
     * @var string - The response as returned from the AgeChecked
     */
    private string $rawResponse = '';

    /**
     * @var array - the response data once it has been through processVerificationResponse()
     */
    private array $response = [];

    /**
     * Authenticate the account with the AVS provider
     * @param array $credentials
     *  - secretKey
     * @return bool
     * @throws InvalidAgeCheckedCredentialsException
     */
    public function authenticate(array $credentials): bool
    {
        if (!array_key_exists('secretKey', $credentials) ||
        empty($credentials['secretKey'])) {
            throw new InvalidAgeCheckedCredentialsException();
        }
        $this->secretKey = $credentials['secretKey'];
        return true;
    }

    /**
     * Build the payload data for submission
     * @param AvsDataSubjectInterface $dataSubject
     * @return self - an instance of the class to allow method chaining
     */
    public function buildAvsPayload(AvsDataSubjectInterface $dataSubject): self
    {
        $this->payload = [
            'merchantSecretKey' => $this->secretKey,
            'name' => $dataSubject->getFirstName(),
            'surname' => $dataSubject->getSurname(),
            'building' => $dataSubject->getBuilding(),
            'street' => $dataSubject->getStreet(),
            'postCode' => $dataSubject->getPostalCode(),
            'countryCode' => $dataSubject->getCountry(),
            'email' => $dataSubject->getEmail()
        ];

        // check if we can get this data from the data subject object
        if (in_array('getState', get_class_methods($dataSubject))) {
            if (!empty($dataSubject->getState())) {
                $this->payload['state'] = $dataSubject->getState();
            }
        }

        // check if we can get this data from the data subject object
        if (in_array('getDriversLicense', get_class_methods($dataSubject))) {
            if (!empty($dataSubject->getDriversLicense())) {
                $this->payload['dlicNo'] = $dataSubject->getDriversLicense();
            }
        }
        return $this;
    }

    /**
     * Get the state of the current AVS payload.
     * Used for debugging as advised if InvalidPayloadException is thrown
     * @return array
     * @see InvalidPayloadException
     */
    public function getAvsPayload()
    {
        return $this->payload;
    }

    /**
     * Submit the age verification request
     * @return $this - return itself to allow method chaining
     * @throws InvalidPayloadException
     */
    public function submitAgeVerification(): self
    {
        // additional check in case the user doesn't perform the check
        if (!($this->payloadIsValid())) {
            throw new InvalidPayloadException();
        }

        $payloadJson = json_encode($this->payload);
        $ch = curl_init($this->endpoint . $this->getEndpointSuffix());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payloadJson);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($payloadJson)));

        // response from AgeChecked - we can process that later
        $this->rawResponse = curl_exec($ch);
        curl_close($ch);

        return $this;
    }

    /**
     * @param string $rawResponse - the raw response (can be obtained from getRawResponse())
     * @return $this
     */
    public function processVerificationResponse(string $rawResponse = ''): self
    {
        $this->response = json_decode($rawResponse, true);
        if ($this->response['authenticated']
            && in_array($this->response['avstatus']['status'], $this->validAvsStatuses)) {
            $this->ageVerified = true;
        }
        return $this;
    }

    /**
     * Determine the correct suffix
     * @return string
     */
    public function getEndpointSuffix()
    {
        if (array_key_exists('dlicNo', $this->payload)) {
            return 'ccdlic';
        } else {
            return 'cceroll';
        }
    }

    /**
     * Check if the payload data are valid enough to submit without issues.
     * Required fields are:
     *  - merchantSecretKey
     *  - name
     *  - surname
     *  - building
     *  - street
     *  - postCode
     *  - countryCode
     *  - email
     * If one of these is missing or empty, then the payload is not valid
     * @return bool
     */
    public function payloadIsValid(): bool
    {
        $validPayload = false; // assume the worst

        if (!empty($this->payload) &&
            (array_key_exists('merchantSecretKey', $this->payload) && !empty($this->payload['merchantSecretKey'])) &&
            (array_key_exists('name', $this->payload) && !empty($this->payload['name'])) &&
            (array_key_exists('surname', $this->payload) && !empty($this->payload['surname'])) &&
            (array_key_exists('building', $this->payload) && !empty($this->payload['building'])) &&
            (array_key_exists('street', $this->payload) && !empty($this->payload['street'])) &&
            (array_key_exists('postCode', $this->payload) && !empty($this->payload['postCode'])) &&
            (array_key_exists('countryCode', $this->payload) && !empty($this->payload['countryCode'])) &&
            (array_key_exists('email', $this->payload) && !empty($this->payload['email']))
        ) {
            $validPayload = true;
        }

        return $validPayload;
    }

    /**
     * @return bool - whether the data subject has been verified
     */
    public function isAgeVerified(): bool
    {
        return $this->ageVerified;
    }

    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
