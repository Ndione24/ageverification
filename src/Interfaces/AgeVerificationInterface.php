<?php
/**
 * Interfaces for the individual age verification services.
 * Allows them to be interchangeable with minimal effort
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification\Interfaces;


interface AgeVerificationInterface
{
    public function authenticate(array $credentials): bool;
    public function buildAvsPayload(AvsDataSubjectInterface $dataSubject): self;
    public function getAvsPayload();
    public function submitAgeVerification(): self;
    public function processVerificationResponse(): self;
    public function isAgeVerified(): bool;
    public function payloadIsValid(): bool;
}
