<?php
/**
 * Test that the functionality in GenericDataSubject work - mainly getters and setters
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package AgeVerification
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\AgeVerification\Tests\Generics;

use GaryBell\AgeVerification\GenericDataSubject;
use PHPUnit\Framework\TestCase;

class GenericDataSubjectTest extends TestCase
{
    private static GenericDataSubject $dataSubject;

    public function testEmptyConstruct()
    {
        $dataSubject = new GenericDataSubject();
        $this->assertEmpty($dataSubject->getFirstName());
        $this->assertEmpty($dataSubject->getSurname());
        $this->assertEmpty($dataSubject->getBuilding());
        $this->assertEmpty($dataSubject->getStreet());
        $this->assertEmpty($dataSubject->getPostalCode());
        $this->assertEmpty($dataSubject->getEmail());
    }

    public function testFirstNameAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getFirstName());
        $dataSubject->setFirstName('Tony');
        $this->assertEquals('Tony', $dataSubject->getFirstName());
    }

    public function testSurnameAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getSurname());
        $dataSubject->setSurname('Stark');
        $this->assertEquals('Stark', $dataSubject->getSurname());
    }

    public function testBuildingAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getBuilding());
        $dataSubject->setBuilding('890');
        $this->assertEquals('890', $dataSubject->getBuilding());
    }

    public function testStreetAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getStreet());
        $dataSubject->setStreet('Fifth Avenue');
        $this->assertEquals('Fifth Avenue', $dataSubject->getStreet());
    }

    public function testPostalCodeAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getPostalCode());
        $dataSubject->setPostalCode('10021');
        $this->assertEquals('10021', $dataSubject->getPostalCode());
    }

    public function testEmailAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getEmail());
        $dataSubject->setEmail('ironman@avengers.org');
        $this->assertEquals('ironman@avengers.org', $dataSubject->getEmail());
    }

    public function testCountryAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getCountry());
        $dataSubject->setCountry('US');
        $this->assertEquals('US', $dataSubject->getCountry());
    }

    public function testDateOfBirthAttribute()
    {
        $dataSubject = clone(self::$dataSubject);
        $this->assertEmpty($dataSubject->getDateOfBirth());
        $dataSubject->setDateOfBirth('1970-05-29');
        $this->assertEquals('1970-05-29', $dataSubject->getDateOfBirth());
    }

    public static function setUpBeforeClass(): void
    {
        self::$dataSubject = new GenericDataSubject();
    }
}
